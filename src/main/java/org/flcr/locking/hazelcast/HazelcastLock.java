package org.flcr.locking.hazelcast;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;

public class HazelcastLock {

	private final String lockName;
	private final HazelcastInstance hazelcast;
	private ILock lock;
	
	HazelcastLock(String lockName, HazelcastInstance hazelcast) {
		this.lockName = lockName;
		this.hazelcast = hazelcast;
	}
	
	void lock() throws Exception {
		lock = hazelcast.getLock(lockName);
		lock.lock();
	}
	
	void unlock() throws Exception {
		lock.unlock();
	}
	
}
