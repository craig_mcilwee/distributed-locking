package org.flcr.locking.hazelcast;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.slf4j.Logger;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICountDownLatch;

class Coordinator extends org.flcr.locking.common.Coordinator {
	
	static {
		org.apache.log4j.Logger.getLogger("com.hazelcast").setLevel(Level.WARN);
	}
	
	private ICountDownLatch latch;
	private HazelcastInstance hc;
	
	Coordinator(boolean useSameLock) {
		super(useSameLock, Worker.class);
	}

	@Override
	protected void beforeRun() {
		Logger log = getLog();
		
		Config config = new Config();
		config.setProperty("hazelcast.logging.type", "slf4j");
		
		log.info("Joining cluster");
		hc = Hazelcast.newHazelcastInstance(config);
		log.info("Joined cluster");
		
		log.info("Getting latch");
		latch = hc.getCountDownLatch("latch");
		if (latch.trySetCount(getWorkerCount() + 1)) {
			log.info("Latch count set");
		} else {
			log.info("Latch count was already set");
		}
	}
	
	@Override
	protected void afterWorkerLaunch() throws Exception {
		latch.countDown();
		latch.await(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
		getLog().info("Latch released");
	}
	
	@Override
	protected void afterWorkersComplete() {
		hc.shutdown();
	}

}
