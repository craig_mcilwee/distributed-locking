package org.flcr.locking.hazelcast;

public class NonConflictingCoordinator extends Coordinator {

	NonConflictingCoordinator() {
		super(false);
	}

	public static void main(String[] args) throws Exception {
		new NonConflictingCoordinator().run();
	}

}
