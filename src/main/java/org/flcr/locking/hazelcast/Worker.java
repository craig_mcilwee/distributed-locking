package org.flcr.locking.hazelcast;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.flcr.locking.common.AbstractWorker;
import org.slf4j.Logger;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ICountDownLatch;

class Worker extends AbstractWorker {
	
	static {
		org.apache.log4j.Logger.getLogger("com.hazelcast").setLevel(Level.WARN);
	}
	
	private HazelcastInstance hc;
	private HazelcastLock lock;
	
	Worker(WorkerConfig config) {
		super(config);
	}
	
	@Override
	protected void beforeRun() throws Exception {
		Logger log = getLog();

		Config config = new Config();
		config.setProperty("hazelcast.logging.type", "slf4j");
		
		log.info("Joining cluster");
		hc = Hazelcast.newHazelcastInstance(config);
		log.info("Joined cluster");
		
		log.info("Getting latch");
		ICountDownLatch latch = hc.getCountDownLatch("latch");

		latch.countDown();
		latch.await(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
		log.info("Latch released");
	}

	@Override
	protected void afterRun() throws Exception {
		hc.shutdown();
	}
	
	
	@Override
	protected void lock(String lockName) throws Exception {
		lock = new HazelcastLock(lockName, hc);
		lock.lock();
	}
	
	@Override
	protected void unlock() throws Exception {
		lock.unlock();
	}
	
	public static void main(String[] args) throws Exception {
		new Worker(WorkerConfig.fromArgs(args)).run();
	}

}
