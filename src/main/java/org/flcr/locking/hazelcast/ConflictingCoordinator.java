package org.flcr.locking.hazelcast;

public class ConflictingCoordinator extends Coordinator {
	
	ConflictingCoordinator() {
		super(true);
	}

	public static void main(String[] args) throws Exception {
		new ConflictingCoordinator().run();
	}

}
