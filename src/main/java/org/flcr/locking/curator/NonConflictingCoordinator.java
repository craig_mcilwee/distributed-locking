package org.flcr.locking.curator;

import org.flcr.locking.common.Coordinator;

public class NonConflictingCoordinator extends Coordinator {

	NonConflictingCoordinator() {
		super(false, Worker.class);
	}

	public static void main(String[] args) throws Exception {
		new NonConflictingCoordinator().run();
	}

}
