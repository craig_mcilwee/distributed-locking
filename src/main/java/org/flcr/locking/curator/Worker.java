package org.flcr.locking.curator;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessLock;
import org.apache.curator.framework.recipes.locks.InterProcessSemaphoreMutex;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.curator.retry.RetryForever;
import org.apache.log4j.Level;
import org.flcr.locking.common.AbstractWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Worker extends AbstractWorker implements ConnectionStateListener {
	
	static {
		org.apache.log4j.Logger.getLogger("org.apache").setLevel(Level.WARN);
	}
	
	private InterProcessLock lock;
	private volatile boolean lockAcquired;
	private final Logger log;
	
	Worker(WorkerConfig config) {
		super(config);
		log = LoggerFactory.getLogger(getClass());
	}
	
	@Override
	protected synchronized void lock(String lockName) throws Exception {
		if (!lockName.startsWith("/")) {
			lockName = "/" + lockName;
		}

		CuratorFramework client = CuratorFrameworkFactory.newClient("localhost:2181,localhost:2182,localhost:2183",
				new RetryForever(500));
		client.getConnectionStateListenable().addListener(this);
		client.start();
		lock = new InterProcessSemaphoreMutex(client, lockName);
		lock.acquire();
		lockAcquired = true;
	}
	
	@Override
	protected synchronized void unlock() throws Exception {
		if (lockAcquired) {
			lock.release();
			lockAcquired = false;
		}
	}
	
	@Override
	public synchronized void stateChanged(CuratorFramework client, ConnectionState newState) {
		switch (newState) {
			case LOST:
				// TODO: is there anything we can really do about this?
				log.error("Lost has been lost");
				break;
			case SUSPENDED:
				log.warn("Lost may have been lost");
				break;
			case RECONNECTED:
				if (lockAcquired) {
					log.info("Lock regained");
				}
				break;
			case CONNECTED:
				// fall through
			case READ_ONLY:
				// fall through
			default:
				break;		
		}
	}
	
	public static void main(String[] args) throws Exception {
		new Worker(WorkerConfig.fromArgs(args)).run();
	}

}
