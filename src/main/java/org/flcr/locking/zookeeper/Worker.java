package org.flcr.locking.zookeeper;

import org.apache.log4j.Level;
import org.flcr.locking.common.AbstractWorker;

public class Worker extends AbstractWorker {
	
	static {
		org.apache.log4j.Logger.getLogger("org.apache").setLevel(Level.WARN);
	}
	
	private ZooKeeperLock lock;
	
	Worker(WorkerConfig config) {
		super(config);
	}
	
	@Override
	protected void lock(String lockName) throws Exception {
		lock = new ZooKeeperLock(lockName);
		lock.lock();
	}
	
	@Override
	protected void unlock() throws Exception {
		lock.unlock();
	}
	
	public static void main(String[] args) throws Exception {
		new Worker(WorkerConfig.fromArgs(args)).run();
	}

}
