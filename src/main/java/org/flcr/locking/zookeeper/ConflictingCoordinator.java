package org.flcr.locking.zookeeper;

import org.flcr.locking.common.Coordinator;

public class ConflictingCoordinator extends Coordinator {
	
	ConflictingCoordinator() {
		super(true, Worker.class);
	}

	public static void main(String[] args) throws Exception {
		new ConflictingCoordinator().run();
	}

}
