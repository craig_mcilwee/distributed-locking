package org.flcr.locking.zookeeper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException.NodeExistsException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Splitter;

class ZooKeeperLock implements Watcher {

	private static final byte[] NO_DATA = new byte[0];
	
	private final String lockName;
	private final Semaphore connectSemaphore;
	private final Logger log;
	private final Semaphore watchSemaphore;
	private ZooKeeper zk;
	private String lockPath;
	private volatile String nextLowestLock;
	
	ZooKeeperLock(String lockName) {
		this.lockName = lockName;
		this.connectSemaphore = new Semaphore(0);
		log = LoggerFactory.getLogger(getClass().getSimpleName() + "[" + lockName + "]");
		watchSemaphore = new Semaphore(0);
	}

	void lock() throws Exception {
		// create the ZK instance and wait for the connection notification
		log.info("Connecting");
		connect();
		connectSemaphore.acquire();
		log.info("Connected");

		// create intermediate nodes
		String lockParentPath = createLockPath();
		
		// create the lock node
		lockPath = zk.create(lockParentPath + "/", NO_DATA, ZooDefs.Ids.OPEN_ACL_UNSAFE,
				CreateMode.EPHEMERAL_SEQUENTIAL);

		int thisLockId = Integer.parseInt(lockPath.substring(lockPath.lastIndexOf('/') + 1));
		log.info("Lock ID: {}", thisLockId);
		
		boolean lockOwned = doWeOwnLock(lockParentPath, thisLockId);
		if (lockOwned) {
			log.info("Lock acquired on first try");
			// all done
			return;
		}
		
		// someone else with a lower ID exists, double check
		nextLowestLock = lockParentPath + "/" + String.format("%010d", thisLockId - 1);
		boolean waitingForLock = true;
		while (waitingForLock) {
			if (zk.exists(nextLowestLock, true) == null) {
				log.info("Next lowest lock gone");
				// the lock below us is gone, recheck to see if we now have the lock
				if (doWeOwnLock(lockParentPath, thisLockId)) {
					// yep, we're good
					log.info("Lock acquired");
					waitingForLock = false;
				} // shouldn't happen
			} else {
				// wait for the lower lock to be removed
				log.info("Waiting for notification");
				watchSemaphore.acquire();
			}
			log.info("Loop");
		}
	}
	
	void unlock() throws Exception {
		zk.delete(lockPath, -1);
	}
	
	private void connect() throws Exception {
		zk = new ZooKeeper("localhost:2181,localhost:2182,localhost:2183", 10000, this);
	}
	
	@Override
	public void process(WatchedEvent event) {
		if (EventType.None.equals(event.getType())
				&& KeeperState.SyncConnected.equals(event.getState())) {
			connectSemaphore.release();
			return;
		}
		
		if (nextLowestLock != null && EventType.NodeDeleted.equals(event.getType())) {
			log.info("Next lowest lock notification");
			watchSemaphore.release();
		}
	}
	
	private String createLockPath() throws Exception {
		List<String> parts = Splitter.on('/').splitToList(lockName);
		StringBuilder path = new StringBuilder();
		for (String part : parts) {
			path.append("/");
			path.append(part);
			
			try {
				if (zk.exists(path.toString(), false) != null) {
					log.info("{} node already exists", path);
					continue;
				}
				zk.create(path.toString(), NO_DATA, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
			} catch (Exception e) {
				if (e instanceof NodeExistsException) {
					log.info("{} node already exists (NEE)", path);
				} else {
					throw e;
				}
			}
		}
		return path.toString();
	}
	
	private boolean doWeOwnLock(String lockParentPath, int thisLockId) throws Exception {
		// get all children in our parent node, include the node we created
		List<String> children = zk.getChildren(lockParentPath, false);
		
		// parse and sort the children
		List<Integer> childIds = new ArrayList<>(children.size());
		for (String child : children) {
			childIds.add(Integer.valueOf(child));
		}
		Collections.sort(childIds);
		log.info("Children: {}", childIds);
		
		// we own the lock if our ID is lowest
		return childIds.get(0).equals(thisLockId);
	}
	
	public String getFullLockName() {
		return "/otms/fieldEntity/" + lockName;
	}
	
}
