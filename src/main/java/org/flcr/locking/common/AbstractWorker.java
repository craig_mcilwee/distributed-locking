package org.flcr.locking.common;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractWorker {
	
	static {		
		BasicConfigurator.configure();
		org.apache.log4j.Logger rootLogger = org.apache.log4j.Logger.getRootLogger();
		rootLogger.removeAllAppenders();
		rootLogger.addAppender(new ConsoleAppender(new PatternLayout("%d\t%-5p (%c) %m%n")));
		rootLogger.setLevel(Level.INFO);
	}
	
	private final Logger log;
	private final String lockName;
	private final int sleepTime;
	
	protected AbstractWorker(WorkerConfig config) {
		log = LoggerFactory.getLogger(getClass().getSimpleName() + "[" + config.id + "]");
		this.lockName = config.lockName;
		this.sleepTime = config.sleepTime;
	}

	protected final void run() throws Exception {
		beforeRun();
		
		log.info("Locking {}", lockName);
		lock(lockName);
		try {
			log.info("Got lock {}", lockName);
			Thread.sleep(sleepTime);
		} finally {
			unlock();
			log.info("Released lock {}", lockName);
		}
		
		afterRun();
	}
	
	protected void beforeRun() throws Exception { }
	
	protected void afterRun() throws Exception { }
	
	protected abstract void lock(String lockName) throws Exception;
	
	protected abstract void unlock() throws Exception;
	
	protected Logger getLog() {
		return log;
	}
		
	protected static class WorkerConfig {
		private final int id;
		private final String lockName;
		private final int sleepTime;
		private WorkerConfig(int id, String lockName, int sleepTime) {
			this.id = id;
			this.lockName = lockName;
			this.sleepTime = sleepTime;
		}
		public static WorkerConfig fromArgs(String[] args) {
			int id = Integer.parseInt(args[0]);
			String lockName = args[1];
			int sleepTime = Integer.parseInt(args[2]);
			return new WorkerConfig(id, lockName, sleepTime);
		}
	}

}
