package org.flcr.locking.common;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Coordinator {
	
	static {		
		BasicConfigurator.configure();
		org.apache.log4j.Logger rootLogger = org.apache.log4j.Logger.getRootLogger();
		rootLogger.removeAllAppenders();
		rootLogger.addAppender(new ConsoleAppender(new PatternLayout("%d\t%-5p (Coordinator) %m%n")));
		rootLogger.setLevel(Level.INFO);
	}
	
	private static final int WORKER_COUNT = 4;
	private static final int SLEEP_TIME = 5000;
	
	private final Logger log;
	private final boolean useSameLock;
	private final Class<?> workerClass;
	
	protected Coordinator(boolean useSameLock, Class<? extends AbstractWorker> workerClass) {
		log = LoggerFactory.getLogger(getClass());
		this.useSameLock = useSameLock;
		this.workerClass = workerClass;
	}
	
	protected Logger getLog() {
		return log;
	}
	
	protected int getWorkerCount() {
		return WORKER_COUNT;
	}

	protected final void run() throws Exception {
		beforeRun();
		
		Set<Process> workers = new HashSet<>(WORKER_COUNT);
		for (int i = 0; i < WORKER_COUNT; i++) {
			workers.add(launchWorker(i));
		}
		log.info("Workers launched");
		
		afterWorkerLaunch();
		
		log.info("Waiting for workers to finish");
		for (Process worker : workers) {
			worker.waitFor();
		}
		log.info("All workers finished");
		
		afterWorkersComplete();
	}
	
	protected void beforeRun() throws Exception { }
	
	protected void afterWorkerLaunch() throws Exception { }
	
	protected void afterWorkersComplete() throws Exception { }
	
	private Process launchWorker(int workerId) throws Exception {
		List<String> commandLine = new ArrayList<String>();
		
		String separator = System.getProperty("file.separator");
		String javaBinary = System.getProperty("java.home") + separator + "bin" + separator + "java";
		if (System.getProperty("os.name").toLowerCase().contains("win")) {
			javaBinary += ".exe";
		}
		commandLine.add(javaBinary);
		commandLine.add("-cp");
		commandLine.add(System.getProperty("java.class.path"));
		commandLine.add(workerClass.getName());
		commandLine.add(String.valueOf(workerId));
		String lockName = "otms/fieldEntity/";
		lockName += useSameLock ? "1" : workerId;
		commandLine.add(lockName);
		commandLine.add(String.valueOf(SLEEP_TIME));
		
		ProcessBuilder procBuilder = new ProcessBuilder(commandLine);
		procBuilder.inheritIO();
		return procBuilder.start();
	}

}
