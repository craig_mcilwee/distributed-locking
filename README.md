
# Distributed Locking Proofs of Concept

## Common

### Coordinator

Base class that launches several `Worker` processes which each attempt to gain a lock.  The `Coordinator` tells each `Worker` which lock it should acquire.  The `Worker` will sleep for a period of time while it holds the lock to show that distributed mutual exclusion is working as intended.

### ConflictingCoordinator

Subclass of `Coordinator` that instructs all `Worker`s to use the same lock.  Program output shows that only one `Worker` will hold the lock at any given time and once the lock is released another worker will immediate acquire the lock.  Runtime should be at least `Coordinator.WORKER_COUNT * Coordinator.SLEEP_TIME`.

### NonConflictingCoordinator

Subclass of `Coordinator` that instructs the `Worker`s to use the distinct locks.  Program output shows that the `Worker`s hold their locks concurrently.  Runtime should be just a little more than `Coordinator.SLEEP_TIME`.

## Hazelcast

Uses [Hazelcast](https://hazelcast.org/) in peer to peer mode with multicast auto discovery.  Sources are found in the `org.flcr.locking.hazelcast` package.

Actual lock implementations is in the `HazelcastLock` class.

### Observations

#### *Fault Tolerance*

Hazelcast will continue to function until the last member leaves the cluster.

#### *Complexity*

Obtaining a lock is extremely simple, Hazelcast provides a single method that provides an extended implementation of `java.util.concurrent.Lock`.

#### *Speed*

Joining the Hazelcast cluster typically takes 8 to 9 seconds.  I suspect this is because we are running in peer to peer mode so the new member must coordinate with the other members and data must be repartitioned and rearranged.  Running in client / server mode would probably solve this but has many of its own drawbacks.  This performance hit shouldn't be much to worry about though since the `HazelcastInstance` is thread-safe and should be shared across the entire VM so it's really just a one time thing.

#### *Fairness*

No fairness guarantees are made.

## ZooKeeper

Uses [ZooKeeper](https://zookeeper.apache.org/) with a 3 node ensemble.  Sources are found in the `org.flcr.locking.zookeeper` package and the configuration for each server is in `src/main/resources`.

Locking implementation is in the `ZooKeeperLock` class and is based on the approach outlined [here](https://zookeeper.apache.org/doc/trunk/recipes.html#sc_recipes_Locks).

### Observations

#### *Fault Tolerance*

ZooKeeper requires a quorum for any operation performed, so a 3 node ensemble will only be able to tolerate loss of a single server.  Once the ability to reach quorum is lost all operations block.

#### *Complexity*

A locking algorithm is not built into ZooKeeper, instead you must write your own on top of its file system like API.  While the algorithm is pretty interesting, it is a bit complicated which adds risk.

#### *Speed*

Connecting to a ZooKeeper ensemble and performing operations is extremely fast, presumably because the servers are already running and have initialized by the time the client connects. 

#### *Fairness*

Locking is guaranteed to be fair.

## Curator

Uses [Curator](http://curator.apache.org/) which is a convenience library built on top of ZooKeepr that provides implementations of the [ZooKeeper recipes](https://zookeeper.apache.org/doc/trunk/recipes.html).  Sources are found in the `org.flcr.locking.curator` package and it uses the same server configuration as the ZooKeeper section.


### Observations

#### *Fault Tolerance*

Same as ZooKeeper.

#### *Complexity*

Far simpler than working with ZooKeeper directly, on the level of Hazelcast.  Retry handling suggested by ZooKeeper is built in, although it's not clear how to best handle connection loss.

#### *Speed*

Same as ZooKeeper. 

#### *Fairness*

Same as ZooKeeper.
